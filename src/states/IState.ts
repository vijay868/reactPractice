import { IProduct } from "../models/IProduct";

export interface IState {
    products: IProduct[];
    totalReactPackages?: number;
}