import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';
import logo from '../logo.svg';

const Header: React.SFC = () => {
    return (
        <header className="header">            
            <h1 className="header-title">React Shop</h1>
            <nav>
                <NavLink to="/products" className="header-link" activeClassName="header-link-active">Products</NavLink>&nbsp;|&nbsp;
                <NavLink to="/admin" className="header-link" activeClassName="header-link-active">Admin</NavLink>
            </nav>
        </header>
    );
};

export default Header;