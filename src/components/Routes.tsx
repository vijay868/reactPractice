import * as React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import AdminPage from './AdminPage';
import Header from './HeaderPage';
import ProductsPage from './ProductsPage';

const Routes: React.SFC = () => {
    return (
        <Router>
            <div>
                <Header />
                <Route path="/products" component={ ProductsPage }></Route>
                <Route path="/admin" component={ AdminPage }></Route>
            </div>
        </Router>
    );
};

export default Routes;