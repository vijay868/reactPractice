import * as React from 'react';
import { products } from '../data/ProductsData';
import { IState } from '../states/IState'

class ProductsPage extends React.Component<{}, IState> {
    public constructor(props: {}) {
        super(props);
        this.state = {
            products: []
        };
    }
    
    /*
    synchronous calling
    public componentDidMount() {
        this.setState({ products });

        fetch('https://api.npms.io/v2/search?q=react')
            .then(response => response.json())
            .then(data => this.setState({ totalReactPackages: data.total }))
    }
    */
    /* async calling */
    public async componentDidMount() {
        const response = await fetch('https://api.npms.io/v2/search?q=react');
        const data = await response.json();
        this.setState({ products, totalReactPackages: data.total });
    }

    public render() {
        const { totalReactPackages } = this.state;
        return (
            <div className="page-container">
                <p>Welcome to React shop where you can get all your tools for ReactJs!</p>
                <div>Total React Packages: { totalReactPackages }</div>
                <ul className="product-list">
                    { this.state.products.map(product => (
                        <li key={product.id} className="product-list-item">
                            {product.name}
                        </li>
                    )) }
                </ul>
            </div>
        );
    }
}

export default ProductsPage;

// https://jasonwatmore.com/post/2020/01/27/react-fetch-http-get-request-examples
// https://jasonwatmore.com/post/2020/09/13/react-display-a-list-of-items